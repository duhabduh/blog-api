import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'


export default class ProfilesController {
  public async store({ request, response, auth }: HttpContextContract) {
    const full_name = request.input('full_name')
    const phone = request.input('phone')

    // const newProfile = Profile.create({
    //   fullName: full_name,
    //   phone: phone,
    //   userId: auth.user?.id
    // })

    const authUser = auth.user

    authUser?.related('profile').create({ fullName: full_name, phone: phone })

    return response.created({ message: 'Profile is created!' })
  }
}
