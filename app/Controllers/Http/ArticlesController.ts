import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import Article from 'App/Models/Article'
import ArticleValidator from 'App/Validators/ArticleValidator'

import Tag from 'App/Models/Tag'

interface TagInterface {
  name: string
}

export default class ArticlesController {
  public async index({ response }: HttpContextContract) {
    const articles = await Article.query().preload('author')
    console.log(articles)
    return response.ok({ message: 'success fetch articles', data: articles })
  }


  public async store({ request, response, auth }: HttpContextContract) {
    try {
      const data = await request.validate(ArticleValidator)
      console.log("data", data)
      return "OK";
      const newArt = new Article()
      newArt.title = data.title
      newArt.body = data.body

      let arrTags: string[] = request.input('tags').split(',')
      let tags: TagInterface[] = arrTags.map(el => { return { name: el } })

      let newTags = await Tag.fetchOrCreateMany('name', tags)
      let tagIds: number[] = newTags.map(tag => tag.id)
      // langkah2 menyimpan tag dan merelasikannya
      // 1. string tags split menjadi array
      // 2. loop pada array, dicek satu persatu jika ada di table tags ambil id nya.
      // 3. kalau belum di table tags, maka create tag baru, ambil id nya
      // 4. simpan relasi dengan metode sync array berisi id tags

      const authUser = auth.user
      await authUser?.related('articles').save(newArt)
      await newArt.related('tags').sync(tagIds)
      return response.created({ message: 'article is created!' })
    } catch (error) {
      return response.unprocessableEntity({ message: error.messages })
    }
  }

  public async show({ params, response }: HttpContextContract) {
    const article = await Article.query().preload('tags').preload('author').where('id', params.id).first()

    return response.ok({ message: 'fetched article', data: article })
  }

  public async update({ request, response, params }: HttpContextContract) {
    let article = await Article.findOrFail(params.id)
    article.title = request.input('title')
    article.body = request.input('body')

    let arrTags: string[] = request.input('tags').split(',')
    let tags: TagInterface[] = arrTags.map(el => { return { name: el } })

    let newTags = await Tag.fetchOrCreateMany('name', tags)
    let tagIds: number[] = newTags.map(tag => tag.id)

    await article.related('tags').sync(tagIds)
    await article.save()

    return response.ok({ message: 'updated' })

  }

  public async destroy({ }: HttpContextContract) {
  }
}
